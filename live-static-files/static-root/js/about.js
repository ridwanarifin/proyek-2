$(document).ready(function () {
    VANTA.CLOUDS({
        el: "#about",
        points: 15.00,
        maxDistance: 30.00,
        backgroundColor: 0x07192f,
        spacing: 20.00
    });

    $("h1.uk-article-title").css({ "font-weight": "200" });
    $("img#avatar").ready(function () {
        $("img#avatar").css({ "display": "block" });
    });

    $(window).scroll(function () {
        var height = $(window).scrollTop();
        if (height >= 257) {
            $("button#btnNav").css("border-style", "none");
        } else {
            $("button#btnNav").css({ "border-style": "ridge" });
        }

        if (height >= 620) {
            $("span.percent").show();
            $("h4#skillName").css({ "font-weight": "bold" });
            $('.chart').easyPieChart({
                lineWidth: 15,
                size: 180,
                barColor: 'rgba(85, 239, 196,1.0)',
                trackColor: 'rgba(223, 230, 233,1.0)',
                scaleColor: false,
                animate: 4000,
                onStep: function (from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent) + '%');
                }
            });
        }
    });
});