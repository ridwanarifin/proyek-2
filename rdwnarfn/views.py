from django.core.exceptions import PermissionDenied
from django.shortcuts import render

def index(request):
    context={}
    return render(request,'index.html',context)

def about(request):
    context={}
    return render(request,'about.html',context)

def contact(request):
    context={}
    return render(request,'contact.html',context)

def permission_denied_view(request):
    raise PermissionDenied