import socket

if socket.gethostname() == '217A6':
    from .local import *
else:
    from .production import *
