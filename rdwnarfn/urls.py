from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('contact/', views.contact),
    path('about/', views.about),
    path('', views.index),
    path('nirvana-admin/', admin.site.urls),
    path('403/', views.permission_denied_view),
]
